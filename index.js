const http = require("http");

const port = 4000;

const server = http.createServer((request, response) => {
	if(request.url == '/profile' && request.method == "GET"){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('Welcome to your profile!');
	}
	if(request.url == '/courses' && request.method == "GET"){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end("Here's our courses available");
	//------------------------------------------------------------------------------------------------------
	}
	if(request.url == '/addcourse' && request.method == "POST"){
		response.writeHead(200, {'Content-Type': 'application/json'});
		response.end("Add a course to our resources");
	}
	//------------------------------------------------------------------------------------------------------
	if(request.url == '/updatecourse' && request.method == "PUT"){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end("Update a course to our resources");
	}
	if(request.url == '/Archivecourses' && request.method == "DELETE"){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end("Archive courses to our resources");
	}
	else{
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end("Welcome to Booking System");
	}
});  

server.listen(port);

// console logs
console.log(`Server now accessible at localhost:${port}`);



